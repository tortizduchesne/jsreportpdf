const pdfRoutes = require('./jsreportPDF_routes');
const jsreport = require('jsreport')();

if (process.env.JSREPORT_CLI)
{
 // when the file is required by jsreport-cli, export
 // jsreport instance to make it possible the usage of jsreport-cli
 module.exports = jsreport
} else
{
 // when the file is started with node.js, start the jsreport server normally
 jsreport.init().then(() =>
 {
   console.log('server started..')
 }).catch((e) =>
  {
   // error during startup
   console.error(e.stack)
   process.exit(1)
 });
};

module.exports = function(app, db)
{
  pdfRoutes(app, db);
};
