const request = require('request');

module.exports = function(app, db)
{
  app.post('/createPDF', (req, res) =>
  {
    var pdf = req.body.stuff;
    var data =
    {
      template: {'shortid':'rkJTnK2ce'},
      data: pdf,
      options: {preview:true}
    }
    var options =
    {
      uri: 'http://localhost:9000/api/report',
      method:'POST',
      json: data
    }
    request(options).pipe(res);
  });
};
