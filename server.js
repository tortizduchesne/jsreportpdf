const express = require('express');
const bodyParser = require('body-parser');
const request = require('request');

const app = express();
const port = 8000;

app.use(bodyParser.urlencoded({extended: true}));

require('./api/routes')(app, {});

app.listen(port, () =>
{
  console.log("Follow the White Rabbit.");
});
